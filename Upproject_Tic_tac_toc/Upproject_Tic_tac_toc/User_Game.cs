﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Upproject_Tic_tac_toc
{
    class User_Game
    {
        public static int user1win = 0;
        public static int user2win = 0;

        public void usergame()
        {
            bool repeat = true;
            while (repeat)
            {
                boardNumber boardvalue = new boardNumber();
                Board backboard = new Board();

                int inputo;
                int inputx;

                int quit = 0;


                Console.Clear();
                backboard.GameBoard();

                Console.SetCursorPosition(0, 15);
                Console.Write("(USER - 1) 번호를 입력하세요 : ");// USER1 번호 입력할 곳으로 커서 이동


                Console.SetCursorPosition(0, 20);
                Console.Write("(USER - 2) 번호를 입력하세요 : ");// USER2 번호 입력할 곳으로 커서 이동

                Console.SetCursorPosition(0, 15);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("(USER-1) 번호를 입력하세요 : ");
                Console.ResetColor();
                Console.SetCursorPosition(0, 20);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("(USER-2) 번호를 입력하세요 : ");
                Console.ResetColor();


                for (int order = 0; order < 9; order++)
                {
                    if (order % 2 == 0) //홀수값과 짝수값을 구분하여 순서 정하기.
                    {
                        Console.SetCursorPosition(31, 15);// 입력값 넣는 곳 뒤로 바로 이동
                        int.TryParse(Console.ReadLine(), out inputo); // 입력값 정수만 받을 수 있게하고 입력값 받는다.
                        for (int loop = 1; loop < 10; loop++)//들어온 o값에 따라 1을 넣는다.
                        {
                            if (inputo == loop) { boardvalue.Boardvalue(loop, 0); break; }
                        }
                    }

                    if (order % 2 == 1)
                    {
                        Console.SetCursorPosition(31, 20);// 입력값 넣는 곳 뒤로 바로 이동
                        int.TryParse(Console.ReadLine(), out inputx);
                        for (int loop = 1; loop < 10; loop++)//들어온 x값에 따라 0을 넣는다.
                        {
                            if (inputx == loop) { boardvalue.Boardvalue(0, loop); break; }
                        }
                    }

                    for (int win = 0; win < 4; win = win + 3)
                    {
                        if (boardvalue.numberArray[0, 0] + boardvalue.numberArray[0, 1] + boardvalue.numberArray[0, 2] == win || boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 0] + boardvalue.numberArray[2, 0] == win ||
                           boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[0, 1] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 1] == win ||
                           boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 0] == win || boardvalue.numberArray[1, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[1, 2] == win ||
                           boardvalue.numberArray[2, 0] + boardvalue.numberArray[2, 1] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 0] == win)
                        {
                            if (win == 0)
                            {

                                Console.SetCursorPosition(43, 5);
                                Console.Write(" USER-2가 승리했습니다. 축하합니다");
                                quit = 1;
                                user2win = user2win + 1;
                                break;
                            }
                            else if (win == 3)
                            {

                                Console.SetCursorPosition(43, 5);
                                Console.Write("USER-1가 승리했습니다. 축하합니다");
                                quit = 1;
                                user1win = user1win + 1;
                                break;
                            }

                        }
                        else if (boardvalue.numberArray[0, 0] != 9 && boardvalue.numberArray[0, 1] != 9 && boardvalue.numberArray[0, 2] != 9 &&
                            boardvalue.numberArray[1, 0] != 9 && boardvalue.numberArray[1, 1] != 9 && boardvalue.numberArray[1, 2] != 9 &&
                            boardvalue.numberArray[2, 0] != 9 && boardvalue.numberArray[2, 1] != 9 && boardvalue.numberArray[2, 2] != 9)
                        {
                            Console.SetCursorPosition(43, 5);
                            Console.Write("무승부입니다.");
                            quit = 1;
                            break;
                        }
                    }
                    if (quit == 1) { break; }

                }
                Console.SetCursorPosition(43, 20);
                Console.Write("게임이 끝났습니다. \n\n1.재시작\n2.종료    :  ");
                int end;
                int.TryParse(Console.ReadLine(), out end);
                if (end == 2) { repeat = false; }
            }



            /*
                        //승리조건
                        for (int win = 0; win < 4; win = win + 3)
                        {
                            if (numberArray[0] + numberArray[1] + numberArray[2] == win || numberArray[0] + numberArray[3] + numberArray[6] == win ||
                               numberArray[0] + numberArray[4] + numberArray[8] == win || numberArray[1] + numberArray[4] + numberArray[7] == win ||
                               numberArray[2] + numberArray[5] + numberArray[8] == win || numberArray[3] + numberArray[4] + numberArray[5] == win ||
                               numberArray[6] + numberArray[7] + numberArray[8] == win || numberArray[2] + numberArray[4] + numberArray[6] == win)
                            {
                                if (win == 0)
                                {
                                    Console.SetCursorPosition(43, 5);
                                    Console.Write(" USER-2가 승리했습니다. 축하합니다");
                                }
                                if (win == 3)
                                {
                                    Console.SetCursorPosition(43, 5);
                                    Console.Write("USER-1가 승리했습니다. 축하합니다");
                                }
                            }
                        }
            */
        }
    }
}
    