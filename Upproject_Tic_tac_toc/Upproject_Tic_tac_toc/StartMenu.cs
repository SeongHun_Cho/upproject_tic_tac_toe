﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;
using System.Windows.Markup;

namespace Upproject_Tic_tac_toc
{
    class StartMenu
    {

        public void Menu()//처음 시작 메뉴를 그림
        {
            bool loop = true;
            while (loop)
            {
                Computer_Game computer = new Computer_Game();
                User_Game user = new User_Game();
                ScoreBoard score = new ScoreBoard();
                Console.WriteLine("              Tic tac toc             ");
                Console.WriteLine("\n\n\n\n\n\n\n1. VS Computer\n\n\n\n2. VS User\n\n\n\n3. SCORE\n\n\n\n4. 종료");
                Console.WriteLine("\n\n\n 메뉴를 선택하세요 : ");
                int input;

                int.TryParse(Console.ReadLine(),out input);

                switch (input)
                {
                    case 1:
                        computer.VsComputer();
                        break;
                    case 2:
                        user.usergame();
                        break;
                    case 3:
                        score.scoreboard();
                        break;
                    case 4:
                        loop = false;
                        break;
                }
            }
        }
    }
}
