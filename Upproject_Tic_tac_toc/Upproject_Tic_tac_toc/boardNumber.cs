﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Upproject_Tic_tac_toc
{
    public class boardNumber
    {
      

        // numberArray는 위치나타내는 배열 9일때는 빈공간 0일때는 컴퓨터(유저2) 1일때는 유저1
            public int[,] numberArray = new int[3,3] { { 9, 9, 9 }, { 9, 9, 9 }, { 9, 9, 9 } };

        public void Boardvalue(int circle, int cross)
        {

            switch (circle) //O가 입력된 배열 생성
            {
                case 1:
                    Console.SetCursorPosition(7, 2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[0, 0] = 1;
                    break;
                case 2:
                    Console.SetCursorPosition(17, 2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[0, 1] = 1;
                    break;
                case 3:
                    Console.SetCursorPosition(27, 2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[0, 2] = 1;
                    break;
                case 4:
                    Console.SetCursorPosition(7, 7);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[1, 0] = 1;
                    break;
                case 5:
                    Console.SetCursorPosition(17, 7);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[1, 1] = 1;
                    break;
                case 6:
                    Console.SetCursorPosition(27, 7);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[1, 2] = 1;
                    break;
                case 7:
                    Console.SetCursorPosition(7, 12);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[2, 0] = 1;
                    break;
                case 8:
                    Console.SetCursorPosition(17, 12);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[2, 1] = 1;
                    break;
                case 9:
                    Console.SetCursorPosition(27, 12);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("O");
                    Console.ResetColor();
                    numberArray[2, 2] = 1;
                    break;
                case 0:
                    break;

            }

            switch (cross) //X자 표시하기
            {
                case 1:
                    Console.SetCursorPosition(7, 2);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[0, 0] = 0;
                    break;
                case 2:
                    Console.SetCursorPosition(17, 2);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[0, 1] = 0;
                    break;
                case 3:
                    Console.SetCursorPosition(27, 2);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[0, 2] = 0;
                    break;
                case 4:
                    Console.SetCursorPosition(7, 7);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[1, 0] = 0;
                    break;
                case 5:
                    Console.SetCursorPosition(17, 7);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[1, 1] = 0;
                    break;
                case 6:
                    Console.SetCursorPosition(27, 7);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[1, 2] = 0;
                    break;
                case 7:
                    Console.SetCursorPosition(7, 12);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[2, 0] = 0;
                    break;
                case 8:
                    Console.SetCursorPosition(17, 12);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[2, 1] = 0;
                    break;
                case 9:
                    Console.SetCursorPosition(27, 12);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("X");
                    Console.ResetColor();
                    numberArray[2, 2] = 0;
                    break;
                case 0:
                    break;


            }
        }
            public void TemporarilyValue(int circle, int cross)
            {

                switch (circle) //O가 입력된 배열 생성
                {
                    case 1:
                        numberArray[0, 0] = 1;
                        break;
                    case 2:
                        numberArray[0, 1] = 1;
                        break;
                    case 3:
                        numberArray[0, 2] = 1;
                        break;
                    case 4:
                        numberArray[1, 0] = 1;
                        break;
                    case 5:
                        numberArray[1, 1] = 1;
                        break;
                    case 6:
                        numberArray[1, 2] = 1;
                        break;
                    case 7:
                        numberArray[2, 0] = 1;
                        break;
                    case 8:
                        numberArray[2, 1] = 1;
                        break;
                    case 9:
                        numberArray[2, 2] = 1;
                        break;
                    case 0:
                        break;

                }

                switch (cross) //X자 표시하기
                {
                    case 1:
                       
                        numberArray[0, 0] = 0;
                        break;
                    case 2:
                        
                        numberArray[0, 1] = 0;
                        break;
                    case 3:
                        
                        numberArray[0, 2] = 0;
                        break;
                    case 4:
                       
                        numberArray[1, 0] = 0;
                        break;
                    case 5:
                        
                        numberArray[1, 1] = 0;
                        break;
                    case 6:
                       
                        numberArray[1, 2] = 0;
                        break;
                    case 7:
                        
                        numberArray[2, 0] = 0;
                        break;
                    case 8:
                       
                        numberArray[2, 1] = 0;
                        break;
                    case 9:
                        
                        numberArray[2, 2] = 0;
                        break;
                    case 0:
                        break;


                }

            }
    }
}
