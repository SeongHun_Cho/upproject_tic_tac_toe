﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Upproject_Tic_tac_toc
{
    class Computer_Game
    {

        public void VsComputer()
        {
            bool repeat = true;
            while (repeat)
            {
                boardNumber boardvalue = new boardNumber();
                Random random = new Random();
                bool flag = true; //컴퓨터 랜덤자리 놓기위한 부울
                int inputo; //유저가 입력한 값
                int randomgaro, randomsero;
                Console.Write("1. O     2. X\n\nO,X 둘 중 하나를 고르세요. : ");
                int ox = Convert.ToInt16(Console.ReadLine());

                if (ox == 1) // ox 둘 중 하나 고름
                {
                    Console.Clear();
                    Board backBoard = new Board();
                    int quit = 0;


                    backBoard.GameBoard();
                    Console.SetCursorPosition(0, 15);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("USER 번호를 입력하세요 : ");
                    Console.ResetColor();




                    for (int sunser = 0; sunser < 9; sunser++)  //홀수 짝수 번갈아서 차례대로 순서 진행
                    {
                        flag = true;
                        if (sunser % 2 == 0)  //짝수 때 유저가 입력
                        {
                            Console.SetCursorPosition(31, 15);// 입력값 넣는 곳 뒤로 바로 이동
                            int.TryParse(Console.ReadLine(), out inputo); // 입력값 정수만 받을 수 있게하고 입력값 받는다.
                            for (int loop = 1; loop < 10; loop++)//들어온 o값에 따라 1을 넣는다.
                            {
                                if (inputo == loop) { boardvalue.Boardvalue(loop, 0); break; }
                            }
                        }


                        if (sunser % 2 == 1)  //홀수 때 컴퓨터가 입력 
                        {
                            if (sunser == 1)
                            {
                                while (flag)
                                {
                                    randomgaro = random.Next(0, 3); randomsero = random.Next(0, 3);
                                    if (boardvalue.numberArray[randomgaro, randomsero] != 1) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } //3x+y+1 이용해서 2차원배열의 1차원화
                                    // 플랙그가 거짓이면 나간다. 브레이크 필요한가 
                                }
                            }
                            else
                            {
                                while (flag) // 랜덤값을 생성하고 나서 그 값이 내가 가정한 상황과 맞는지 비교할것
                                {
                                    randomgaro = random.Next(0, 3); randomsero = random.Next(0, 3);
                                    if (boardvalue.numberArray[randomgaro, randomsero] == 9)
                                    {
                                        boardvalue.TemporarilyValue(0, (3 * randomgaro) + randomsero + 1);



                                        //여러개가 동시에 2개로 인식될 때의 경우와 하나의 줄이 인식 될 때의 값
                                        if (boardvalue.numberArray[0, 0] == 1 && boardvalue.numberArray[0, 1] == 1 && boardvalue.numberArray[1, 1] == 1) { if (boardvalue.numberArray[2, 1] == 0 || boardvalue.numberArray[2, 2] == 0 || boardvalue.numberArray[0, 2] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } } //첫번째로 두가지 빙고가 겹치는 경우
                                        else if (boardvalue.numberArray[0, 1] == 1 && boardvalue.numberArray[0, 2] == 1 && boardvalue.numberArray[1, 1] == 1) { if (boardvalue.numberArray[2, 1] == 0 || boardvalue.numberArray[2, 0] == 0 || boardvalue.numberArray[0, 0] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[1, 1] == 1 && boardvalue.numberArray[2, 0] == 1 && boardvalue.numberArray[2, 1] == 1) { if (boardvalue.numberArray[0, 1] == 0 || boardvalue.numberArray[0, 2] == 0 || boardvalue.numberArray[2, 2] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[1, 1] == 1 && boardvalue.numberArray[2, 2] == 1 && boardvalue.numberArray[2, 1] == 1) { if (boardvalue.numberArray[0, 0] == 0 || boardvalue.numberArray[2, 0] == 0 || boardvalue.numberArray[0, 1] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[0, 0] == 1 && boardvalue.numberArray[1, 0] == 1 && boardvalue.numberArray[1, 1] == 1) { if (boardvalue.numberArray[2, 0] == 0 || boardvalue.numberArray[2, 2] == 0 || boardvalue.numberArray[1, 2] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[1, 1] == 1 && boardvalue.numberArray[2, 0] == 1 && boardvalue.numberArray[2, 1] == 1) { if (boardvalue.numberArray[0, 1] == 0 || boardvalue.numberArray[0, 2] == 0 || boardvalue.numberArray[2, 2] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[0, 2] == 1 && boardvalue.numberArray[1, 1] == 1 && boardvalue.numberArray[1, 2] == 1) { if (boardvalue.numberArray[1, 0] == 0 || boardvalue.numberArray[2, 0] == 0 || boardvalue.numberArray[2, 2] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[1, 1] == 1 && boardvalue.numberArray[2, 0] == 1 && boardvalue.numberArray[1, 0] == 1) { if (boardvalue.numberArray[0, 0] == 0 || boardvalue.numberArray[0, 2] == 0 || boardvalue.numberArray[1, 2] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[1, 1] == 1 && boardvalue.numberArray[2, 2] == 1 && boardvalue.numberArray[1, 2] == 1) { if (boardvalue.numberArray[0, 0] == 0 || boardvalue.numberArray[0, 2] == 0 || boardvalue.numberArray[1, 1] == 0) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; } }
                                        else if (boardvalue.numberArray[0, 0] + boardvalue.numberArray[0, 1] + boardvalue.numberArray[0, 2] != 11 &&
                                                 boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 0] + boardvalue.numberArray[2, 0] != 11 &&
                                                 boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 2] != 11 &&
                                                 boardvalue.numberArray[0, 1] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 1] != 11 &&
                                                 boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 2] + boardvalue.numberArray[2, 2] != 11 &&
                                                 boardvalue.numberArray[1, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[1, 2] != 11 &&
                                                 boardvalue.numberArray[2, 0] + boardvalue.numberArray[2, 1] + boardvalue.numberArray[2, 2] != 11 &&
                                                 boardvalue.numberArray[2, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[0, 2] != 11) { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; }
                                        //else { boardvalue.Boardvalue(0, (3 * randomgaro) + randomsero + 1); flag = false; }
                                        if (flag == true) { boardvalue.numberArray[randomgaro, randomsero] = 9; }
                                    }


                                    
                                }
                            }
                        }

                        for (int win = 0; win < 4; win = win + 3)
                        {
                            if (boardvalue.numberArray[0, 0] + boardvalue.numberArray[0, 1] + boardvalue.numberArray[0, 2] == win || boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 0] + boardvalue.numberArray[2, 0] == win ||
                               boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[0, 1] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 1] == win ||
                               boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 2] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[1, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[1, 2] == win ||
                               boardvalue.numberArray[2, 0] + boardvalue.numberArray[2, 1] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 0] == win)
                            {
                                if (win == 0)
                                {

                                    Console.SetCursorPosition(43, 5);
                                    Console.Write(" COMPUTER가 승리했습니다. 축하합니다");
                                    quit = 1;

                                    break;
                                }
                                else if (win == 3)
                                {

                                    Console.SetCursorPosition(43, 5);
                                    Console.Write("USER-1가 승리했습니다. 축하합니다");
                                    quit = 1;

                                    break;
                                }

                            }
                        }
                            if (boardvalue.numberArray[0, 0] != 9 && boardvalue.numberArray[0, 1] != 9 && boardvalue.numberArray[0, 2] != 9 && //이건 좀 아닌것 같은데
                                boardvalue.numberArray[1, 0] != 9 && boardvalue.numberArray[1, 1] != 9 && boardvalue.numberArray[1, 2] != 9 &&
                                boardvalue.numberArray[2, 0] != 9 && boardvalue.numberArray[2, 1] != 9 && boardvalue.numberArray[2, 2] != 9)
                            {
                                Console.SetCursorPosition(43, 5);
                                Console.Write("무승부입니다.");
                                quit = 1;
                                break;
                            }
                            if (quit == 1) { break; }
                        
                        //if (quit == 1) { break; }
                    }

                }

                if (ox == 2) // ox 둘 중 하나 고름
                {
                    Console.Clear();
                    Board backBoard = new Board();
                    int inputx;
                    int quit = 0;
                    backBoard.GameBoard();
                    Console.SetCursorPosition(0, 15);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("USER 번호를 입력하세요 : ");
                    Console.ResetColor();




                    for (int sunser = 0; sunser < 9; sunser++)
                    {
                        flag = true;
                        if (sunser % 2 == 1)
                        {
                            Console.SetCursorPosition(31, 15);// 입력값 넣는 곳 뒤로 바로 이동
                            int.TryParse(Console.ReadLine(), out inputx); // 입력값 정수만 받을 수 있게하고 입력값 받는다.
                            for (int loop = 1; loop < 10; loop++)//들어온 o값에 따라 1을 넣는다.
                            {
                                if (inputx == loop) { boardvalue.Boardvalue(0, loop); break; }
                            }
                        }
                        

                        if (sunser % 2 == 0)
                        {
                            if (sunser == 0)
                            {
                                while (flag)
                                {
                                    randomgaro = random.Next(0, 3); randomsero = random.Next(0, 3);
                                    if (boardvalue.numberArray[randomgaro, randomsero] != 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } //3x+y+1 이용해서 2차원배열의 1차원화
                                                                                                                                                                          // 플랙그가 거짓이면 나간다. 브레이크 필요한가 
                                }
                            }
                            else
                            {
                                while (flag) // 랜덤값을 생성하고 나서 그 값이 내가 가정한 상황과 맞는지 비교할것
                                {
                                    randomgaro = random.Next(0, 3); randomsero = random.Next(0, 3);
                                    if (boardvalue.numberArray[randomgaro, randomsero] == 9)
                                    {
                                        boardvalue.TemporarilyValue((3 * randomgaro) + randomsero + 1, 0);
                                        



                                            if (boardvalue.numberArray[0, 0] == 0 && boardvalue.numberArray[0, 1] == 0 && boardvalue.numberArray[1, 1] == 0) { if (boardvalue.numberArray[2, 1] == 1 || boardvalue.numberArray[2, 2] == 1 || boardvalue.numberArray[0, 2] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } } //첫번째로 두가지 빙고가 겹치는 경우
                                            else if (boardvalue.numberArray[0, 1] == 0 && boardvalue.numberArray[0, 2] == 0 && boardvalue.numberArray[1, 1] == 0) { if (boardvalue.numberArray[2, 1] == 1 || boardvalue.numberArray[2, 0] == 1 || boardvalue.numberArray[0, 0] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[1, 1] == 0 && boardvalue.numberArray[2, 0] == 0 && boardvalue.numberArray[2, 1] == 0) { if (boardvalue.numberArray[0, 1] == 1 || boardvalue.numberArray[0, 2] == 1 || boardvalue.numberArray[2, 2] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[1, 1] == 0 && boardvalue.numberArray[2, 2] == 0 && boardvalue.numberArray[2, 1] == 0) { if (boardvalue.numberArray[0, 0] == 1 || boardvalue.numberArray[2, 0] == 1 || boardvalue.numberArray[0, 1] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[0, 0] == 0 && boardvalue.numberArray[1, 0] == 0 && boardvalue.numberArray[1, 1] == 0) { if (boardvalue.numberArray[2, 0] == 1 || boardvalue.numberArray[2, 2] == 1 || boardvalue.numberArray[1, 2] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[1, 1] == 0 && boardvalue.numberArray[2, 0] == 0 && boardvalue.numberArray[2, 1] == 0) { if (boardvalue.numberArray[0, 1] == 1 || boardvalue.numberArray[0, 2] == 1 || boardvalue.numberArray[2, 2] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[0, 2] == 0 && boardvalue.numberArray[1, 1] == 0 && boardvalue.numberArray[1, 2] == 0) { if (boardvalue.numberArray[1, 0] == 1 || boardvalue.numberArray[2, 0] == 1 || boardvalue.numberArray[2, 2] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[1, 1] == 0 && boardvalue.numberArray[2, 0] == 0 && boardvalue.numberArray[1, 0] == 0) { if (boardvalue.numberArray[0, 0] == 1 || boardvalue.numberArray[0, 2] == 1 || boardvalue.numberArray[1, 2] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[1, 1] == 0 && boardvalue.numberArray[2, 2] == 0 && boardvalue.numberArray[1, 2] == 0) { if (boardvalue.numberArray[0, 0] == 1 || boardvalue.numberArray[0, 2] == 1 || boardvalue.numberArray[1, 1] == 1) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; } }
                                            else if (boardvalue.numberArray[0, 0] + boardvalue.numberArray[0, 1] + boardvalue.numberArray[0, 2] != 9 &&
                                                     boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 0] + boardvalue.numberArray[2, 0] != 9 &&
                                                     boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 2] != 9 &&
                                                     boardvalue.numberArray[0, 1] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 1] != 9 &&
                                                     boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 2] + boardvalue.numberArray[2, 2] != 9 &&
                                                     boardvalue.numberArray[1, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[1, 2] != 9 &&
                                                     boardvalue.numberArray[2, 0] + boardvalue.numberArray[2, 1] + boardvalue.numberArray[2, 2] != 9 &&
                                                     boardvalue.numberArray[2, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[0, 2] != 9) { boardvalue.Boardvalue((3 * randomgaro) + randomsero + 1, 0); flag = false; }
                                        
                                    }

                                    if (flag == true) { boardvalue.numberArray[randomgaro, randomsero] = 9; }
                                }
                            }
                        }

                        for (int win = 0; win < 4; win = win + 3)
                        {
                            if (boardvalue.numberArray[0, 0] + boardvalue.numberArray[0, 1] + boardvalue.numberArray[0, 2] == win || boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 0] + boardvalue.numberArray[2, 0] == win ||
                               boardvalue.numberArray[0, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[0, 1] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 1] == win ||
                               boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 2] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[1, 0] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[1, 2] == win ||
                               boardvalue.numberArray[2, 0] + boardvalue.numberArray[2, 1] + boardvalue.numberArray[2, 2] == win || boardvalue.numberArray[0, 2] + boardvalue.numberArray[1, 1] + boardvalue.numberArray[2, 0] == win)
                            {
                                if (win == 0)
                                {

                                    Console.SetCursorPosition(43, 5);
                                    Console.Write(" USER-1이 승리했습니다. 축하합니다");
                                    quit = 1;

                                    break;
                                }
                                else if (win == 3)
                                {

                                    Console.SetCursorPosition(43, 5);
                                    Console.Write("COMPUTER가 승리했습니다. 축하합니다");
                                    quit = 1;

                                    break;
                                }

                            }
                            else if (boardvalue.numberArray[0, 0] != 9 && boardvalue.numberArray[0, 1] != 9 && boardvalue.numberArray[0, 2] != 9 &&
                                boardvalue.numberArray[1, 0] != 9 && boardvalue.numberArray[1, 1] != 9 && boardvalue.numberArray[1, 2] != 9 &&
                                boardvalue.numberArray[2, 0] != 9 && boardvalue.numberArray[2, 1] != 9 && boardvalue.numberArray[2, 2] != 9)
                            {
                                Console.SetCursorPosition(43, 5);
                                Console.Write("무승부입니다.");
                                quit = 1;
                                break;
                            }

                        }
                        if (quit == 1) { break; }
                    }



                }
                Console.SetCursorPosition(43, 20);
                Console.Write("게임이 끝났습니다. \n\n1.재시작\n2.종료    :  ");
                int end;
                int.TryParse(Console.ReadLine(), out end);
                if (end == 2) { repeat = false; }

            }
        }
        }
    }

